package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (isValid(start, end)) {
            return generateTable(start,end);
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        return isStartNotBiggerThanEnd(start,end) && isInRange(start) && isInRange(end);
    }

    public Boolean isInRange(int number) {
        return number <= 1000 && number >= 1;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i <= end; i++) {
            sb.append(generateLine(start,i));
            if(i<end){
                String format = String.format("%n");
                sb.append(format);
            }

        }
        return sb.toString();
    }

    public String generateLine(int start, int row) {
        StringBuilder sb= new StringBuilder();
        for(int i = start; i <= row; i++) {
            sb.append(generateSingleExpression(i,row));
            if(i < row) {
                sb.append("  ");
            }
        }
        return sb.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" + multiplier +
                "=" + multiplicand * multiplier;
    }
}
